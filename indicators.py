#+--------------------------------------+
#| Indicators                           |
#+--------------------------------------+

# Outputs Donchian Prices
def Donchian(high, low, period):
    import pandas as pd
    donchian = pd.DataFrame(index=high.index)
    donchian['H'] = high.rolling(window=period,center=False).max()
    donchian['L'] = low.rolling(window=period,center=False).min()
    return donchian

# MACD
def MACDHist(series, *MACDparam):
    macd = series.ewm(span=MACDparam[0], adjust=False).mean() - \
           series.ewm(span=MACDparam[1], adjust=False).mean()
    macd = macd - macd.ewm(span=MACDparam[2], adjust=False).mean()
    return macd

# Moving Average Crossover
def MACross(series, Fast, Slow):
    return series.rolling(window=Fast, center=False).mean() - \
           series.rolling(window=Slow, center=False).mean()

# Relative Strength Index
def RSI(series, period=14):
    import pandas as pd

    delta = series.diff()
    dUp, dDown = delta.copy(), delta.copy()
    dUp[dUp < 0] = 0
    dDown[dDown > 0] = 0

    rollUp = dUp.rolling(period).mean()
    rollDown = dDown.rolling(period).mean().abs()

    rs = rollUp / rollDown
    rsi= 100.0 - (100.0 / (1.0 + rs))
    return rsi

# Average True Range
def ATR(high, low, close, period):
    from pandas import concat
    highLow = high-low
    highClose = abs(high-close.shift(1))
    lowClose = abs(low-close.shift(1))
    TR = concat([highLow, highClose, lowClose], axis=1).max(axis=1)
    res = TR.rolling(period).mean()
    return res

# SuperTrend
def SuperTrend(high, low, close, ATRperiod=14, upperMult=3, lowerMult=3):
    import numpy as np, pandas as pd
    import numba as nb
    
    indic = pd.DataFrame(index=high.index)
    
    indic['ATR'] = ATR(high, low, close, ATRperiod)
    indic['upperBasic'] = (high+low)/2 + upperMult*indic['ATR']
    indic['lowerBasic'] = (high+low)/2 - lowerMult*indic['ATR']
    
    @nb.njit
    def make_supertrend(upperBasic, lowerBasic, close):
        lower = np.full_like(lowerBasic, np.nan)
        upper = np.full_like(upperBasic, np.nan)
        sTrend = np.full_like(lowerBasic, np.nan)
        for i in range(len(upperBasic)):
            if not np.isnan(upperBasic[i]):
                if np.isnan(upperBasic[i-1]) or (upperBasic[i] < upper[i-1]) or (close[i-1] > upper[i-1]):
                    upper[i] = upperBasic[i]
                else:
                    upper[i] = upper[i-1]
                if np.isnan(lowerBasic[i-1]) or (lowerBasic[i] > lower[i-1]) or (close[i-1] < lower[i-1]):
                    lower[i] = lowerBasic[i]
                else:
                    lower[i] = lower[i-1]
                if sTrend[i-1] == 1:
                    if close[i] <= upper[i]:
                        sTrend[i] = 1
                    else:
                        sTrend[i] = 0
                else:
                    if close[i-1] >= lower[i-1]:
                        sTrend[i] = 0
                    else:
                        sTrend[i] = 1
                if sTrend[i-1] == 1:
                    lower[i-1] = np.nan
                elif sTrend[i-1] == 0:
                    upper[i-1] = np.nan
                else:
                    lower[i-1] = np.nan
                    upper[i-1] = np.nan
        if sTrend[-1] == 1: lower[-1] = np.nan
        else: upper[-1] = np.nan
        return upper, lower
    indic['upper'], indic['lower'] = make_supertrend(indic.upperBasic.values, indic.lowerBasic.values, close.values)
    return indic[['upper', 'lower']]